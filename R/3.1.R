#in this script we implement algorithm3.1

#' Mode-finding for binary Laplace GPC
#'
#' This is the function which comuptes the posterior mode which maximize the objective function by an iteration,
#' and that is equivalent to maximizing \code{p(f|inputs,target)}. Also it computes an approximation to the log marginal likelihood
#' with help of the already computed posterior mode.
#'
#' In a binary Laplace GPC there is a latent variable f related to the input point, with the help of which the target class praobability
#' can be computed for each input point. This function aims to find \code{argmax{p(f|inputs,target)}} using Newton's Method and gives
#' an estimation of the log marginal likelihood with help of Taylor expansion of \code{p(f|inputs,target)} locally around the already
#' computed posterior mode.\cr
#' \cr
#' \code{cov_matrix} should be a diagonal matrix, \code{nrow} of which should be same as the length of the target\cr
#' \cr
#' This function is not vectorized.
#'
#' @param cov_matrix a numeric matrix which should be the covariance matrix of the traning set. More in \code{Details}
#' @param target a vector containing only -1 and 1 which represents the classes, to which the training points belong
#' @param likelihood_fun a function with two arguments which gives the target class probability as a function of the latent variable f, e.g.
#'   \code{logistic} or \code{cumulative_gaussian}
#' @param iteration an integer scale which defines the number of iterations in the function, Default value is 100.
#' @return a list which contains the posterior mode and the approximation of the log likelihood
#' @examples lap_approx(diag(1,5),c(1,1,-1,1,-1),logistic,1000)
#' @export
lap_approx <- function(cov_matrix,target,likelihood_fun,iteration=100){

  ## Fehlermeldung
  for (i in target){
    stopifnot("target vector can only contain -1 or 1"=i==1||i==-1)
  }
  stopifnot("covariance matrix must be a diagonal matrix"= identical(cov_matrix,t(cov_matrix)))
  stopifnot("the dimension of target and covariance must be same"= nrow(cov_matrix) == length(target))
  stopifnot("invalid likelihood function"=is.function(likelihood_fun))
  stopifnot("invalid number of iterations"= length(iteration)==1 && iteration>=1)


  ## Beginn des Algorithmus
  f <- rep(0,length(target))
  log_p<-function(g){
    return(log(likelihood_fun(target,g)))
  }
  con<-0
  f_all<-list(f)
  for(i in 1:iteration){


    W <- -pracma::hessian(log_p,f)
    W_q <- pracma::sqrtm(W)$B
    L <- t(chol(diag(x =1,nrow = nrow(W)) + W_q%*%cov_matrix%*%W_q))
    b <- W%*%f + pracma::grad(log_p,f)
    a <- b - solve(W_q%*%t(L),solve(L,W_q%*%cov_matrix%*%b))

    con[i] <- -(1/2)*t(a)%*%f + log(likelihood_fun(target,f))
    f_all[[i]]<-f

    f <- cov_matrix%*%a
  }
  f<-f_all[[which.max(con)]]
  log_q <- -(1/2)*t(a)%*%f + log(likelihood_fun(target,f)) -sum(log(diag(L)))
  return(list("mode"=f,"aprox.log.marg.likelihood"=log_q))
}
