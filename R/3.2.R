#algorithm 3.2

#' Predictions for binary Laplace GPC
#'
#' This function gives prediction for the class probability (for class 1) of the \code{test_input}, given training observations, mode,
#' covariance function and likelihood function.
#'
#' With the mode which can be computed by the function \code{lap_approx} the class 1 probability of any test point can be predicted
#' with the help of a prediction od the latent variable f (using GPR).Then with a sigmoid function the predicted latent variable will be again
#' mapping to a value in \code{[0,1]}, and that is the final prediction for the class probability of the test input.\cr
#' \cr
#' The length of \code{mode}, \code{Inputs} must be the same. Each Point in the training set must be of the same dimension.
#'If this dimension is 1 use an atomic vector as \code{Inputs}, otherwise use a
#'list which contains some atomic vectors, each of which represents respectively a
#'high-dimensional training point. If the \code{Inpts} points are one-dimensional
#'use a numeric scale for \code{test_input}, otherwise use a list of length 1
#'which contains an atomic vector representing the high-dimensional test point.\cr
#'\cr
#' This function is noc vectorized.
#'
#'
#'
#'
#' @param mode an atomic vector which represents the posterior mode which can be computed by the function \code{lap_approx}
#' @param Inputs a numeric vector(atomic or list) which should represent the
#'  training set. Use atomic vectors if the training points are one dimensional
#'  and use lists if not.
#' @param target an atomic vector containing -1 and 1 which shows the classes, to which the training points belong
#' @param cov_fun a vectorized function of two arguments
#' @param likelihood_fun a function of two arguments (y,f) which measures the goodness of f of model given value of y
#' @param if.logistic a logical value. the logistic will be used by \code{TRUE} and the cumulative Gaussian will be used otherwise.
#'   Default value is \code{TRUE}
#' @param test_input a scale or a list with length 1, the class probability of which will be
#'  predicted in this function. See more in \code{details}.
#' @return the predictive probability that test outputs belong to class 1
#' @examples pred(1:5,1:5,c(1,1,1,-1,-1),const_cov(1),logistic,6)
#'
#' #an example with high dimensional inputs:
#' inputs<-list(c(1,2,3),c(-1,0,2),c(1,0,3),c(2,2,8),c(3,5,6))
#' pred(rep(-100,5),inputs,rep(1,5),se_cov(5),logistic,list(c(1,2,2)))
#'@export
pred <- function(mode,Inputs,target,cov_fun,likelihood_fun,test_input,if.logistic=TRUE){


  ## Fehlermeldungen

  stopifnot("Your Inputs is empty!"=length(Inputs)>0)
  if(is.list(Inputs)){
    len<-0
    for(i in 1:length(Inputs)){len[i]<-length(Inputs[[i]])}
    stopifnot("All training inputs must be of the same dimension"=length(unique(len))==1)
    if(unique(len)!=1){stopifnot("Please give high dimensional test input as a list of length 1"=is.list(test_input))}
  }
  stopifnot("Please give only one test point!"=length(test_input)==1)
  stopifnot("Training inputs and targets must be of the same length."=length(Inputs)==length(target))
  stopifnot("lengths of mode and target are not compatible"=length(mode)==length(target))
  for(i in Inputs){
    stopifnot("dimension of trainng inputs and test inputs must be same"=length(i)==length(test_input[[1]]))
  }
  for (i in target){
    stopifnot("target vector can only contain -1 or 1"=i==1||i==-1)
  }
  stopifnot("invalid likelihood function"=is.function(likelihood_fun))
  stopifnot("invalid covariance function"=is.function(cov_fun))


## Beginn des Algorithmus
  log_p<-function(g){
    return(log(likelihood_fun(target,g)))
  }
  W <- -pracma::hessian(log_p,mode)
  W_s<-pracma::sqrtm(W)$B
  k_s<-outer(Inputs, test_input, cov_fun)
  L <- t(chol(diag(x =1,nrow = nrow(W)) + W_s%*%outer(Inputs,Inputs,cov_fun)%*%W_s))
  f_s <- as.vector(t(k_s)%*%pracma::grad(log_p,mode))
  v <- solve(L,(W_s%*%k_s))
  V_f <- as.vector(cov_fun(test_input,test_input) - t(v)%*%v)

  if(if.logistic==TRUE){sigma<-function(z){

    return(logistic(z,1)) }
  }
  if(if.logistic==FALSE) {sigma<-function(z){
    eins<-rep(1,length(z))
    return(cumulative_gaussian(z,1))
  }
  }

  integrand <- function(z){
    sigma(z)*dnorm(z,mean = f_s, sd=sqrt(V_f))
  }

  return(stats::integrate(integrand, -Inf,Inf))
}

