


#'constant-covariance function-fabric
#'
#'This function-fabric produces the constant covariance function with the parameter \code{sigma}.
#'
#'Attention: This function only produces covariance functions and can not be directly used as a covariance function
#'
#'@param sigma a numeric scale
#'
#'@return The covariance function which needs two atomic vector arguments with the same length and gives the constant value \code{sigma}
#'for all inputs.\cr
#'\cr
#'The returned function is vectorized can be used in GPR and GPC functions.
#'@examples const_cov(1)
#' const_cov(0.2)(1,3)
#' const_cov(0.5)(list(c(1,2),c(2,3)),list(5:6))
#'@export
const_cov<-function(sigma){
  if(length(sigma)!=1){
    stop("invalid parameter in cov function, sigma must have length 1")
  }
  return(Vectorize(function(x,y){
    if(length(x)!=length(y)){
      stop("inputs of a covariance function must have the same dimension")
    }
    return(sigma)
  }))
}


##########################################

#'linear-covariance function-fabric
#'
#'This function-fabric produces the linear covariance function with the linear parameter \code{sigma}.
#'
#'Attention: This function only produces covariance functions and can not be directly used as a covariance function
#'
#'@param sigma a numeric vector
#'
#'@return The covariance function which needs two atomic vector arguments with the same length. It gives the linear covariance with
#'parameter \code{sigma} if \code{sigma} has the same length with the vector arguments and that with parameter \code{rep(sigma,length(inputs))}
#'if \code{sigma} has the length 1.\cr
#'\cr
#'The returned function is vectorized can be used in GPR and GPC functions.
#'@examples lin_cov(1)
#' lin_cov(0.2)(1,3)
#' lin_cov(1:2)(list(c(1,2),c(2,3)),list(5:6))
#'@export
lin_cov<-function(sigma){
  return(Vectorize(function(x,y){
    if(length(x)!=length(y)){
      stop("inputs of a covariance function must have the same dimension")
    }
    else if(length(x)!=length(sigma) && length(sigma)!=1){
      stop("length of parameter in cov function does not match length of inputs")
    }
    else return(sum(sigma*x*y))
  }))
}

##########################################
#'polynomial-covariance function-fabric
#'
#'This function-fabric produces the polynomial covariance function with the two parameters \code{sigma} and \code{p}.
#'
#'Attention: This function only produces covariance functions and can not be directly used as a covariance function
#'
#'@param sigma a numeric scale. Default value is 0.
#'@param p a positive integer. Default value is 1.
#'@return The covariance function which needs two atomic vector arguments with the same length. It gives the polynomial covariance with
#'parameters \code{sigma} and \code{p} and that is, \code{(dot_prod+sigma)^p} where \code{dot_product} represents the dot product of the two vector inputs.\cr
#'\cr
#'The returned function is vectorized can be used in GPR and GPC functions.
#'@examples poly_cov(1,2)
#' poly_cov(p=2)(1,3)
#' poly_cov(1,3)(list(c(1,2),c(2,3)),list(5:6))
#'@export
poly_cov<-function(sigma=0,p=1){
  if(length(sigma)!=1){
    stop("invalid parameter in cov function, sigma must have length 1")
  }
  else if(length(p)!=1){
    stop("invalid parameter in cov function, p must have length 1")
  }
  else if(p<=0 || p-as.integer(p)!=0){
    stop("invalid parameter in cov function, p must be an positive integer")
  }
  if(length(sigma)!=1){
    stop("invalid parameter in cov function, sigma should have length 1")
  }

  return(Vectorize(function(x,y){
    if(length(x)!=length(y)){
      stop("inputs of a covariance function must have the same dimension")
    }

    else return((sigma+sum(x*y))^p)
  }))

}





##########################################
#'squared-expotential-covariance function-fabric
#'
#'This function-fabric produces the squared expotenial covariance function with the distance parameter \code{l}.
#'
#'Attention: This function only produces covariance functions and can not be directly used as a covariance function
#'
#'@param l a numeric scale. Default value is 1.
#'@return The covariance function which needs two atomic vector arguments with the same length. It gives the squared expotenial covariance with
#' parameter \code{l} and that is, \code{exp(-(r^2)/2*l^2)} where \code{r} represents the distance between the two vector inputs.\cr
#'\cr
#'The returned function is vectorized can be used in GPR and GPC functions.
#'@examples se_cov(0.5)
#' se_cov()(1,3)
#' se_cov(2)(list(c(1,2),c(2,3)),list(5:6))
#'@export
se_cov <- function(l=1){

  if(length(l)!=1){
    stop("invalid parameter in cov function, l must have length 1")
  }

  f <- function(x,y){
    if(length(x)!=length(y)){
      stop("inputs of a covariance function must have the same dimension")
    }
    r <- pracma::Norm(x-y)
    return(exp(-r^2/(2*l^2)))
  }
  return(Vectorize(f))
}

##########################################

#we ignore the Matern COV function here.

##########################################
#'gamma-exponential-covariance function-fabric
#'
#'This function-fabric produces the gamma exponential covariance function with the two parameters \code{l} and \code{gamma}.
#'
#'Attention: This function only produces covariance functions and can not be directly used as a covariance function
#'
#'@param l a numeric scale. Default value is 1.
#'@param gamma a numeric scale. Default value is 1 and value must be in (0,2].
#'@return The covariance function which needs two atomic vector arguments with the same length. It gives the gamma exponential covariance with
#'parameters \code{l} and \code{gamma} and that is, \code{exp(-(r/l)^gamma)} where \code{r} represents the distance between the two vector inputs.\cr
#'\cr
#'The returned function is vectorized can be used in GPR and GPC functions.
#'@examples gamma_exp_cov(0.2,2)
#' gamma_exp_cov(gamma=0.5)(1,3)
#' gamma_exp_cov(5,sqrt(exp(1)))(list(c(1,2),c(2,3)),list(5:6))
#'@export
gamma_exp_cov <- function(l=1,gamma=1){

  if(length(l)!=1){
    stop("invalid parameter in cov function, l must have length 1")
  }
  if(length(gamma)!=1){
    stop("invalid parameter in cov function, gamma must have length 1")
  }

  if(l<=0){
    stop("invalid parameter in cov function, l must be positive")
  }

  if(gamma<=0 || gamma>2){
    stop("invalid parameter in cov function, gamma must be in (0,2]")
  }


  f<-function(x,y){
    if(length(x)!=length(y)){
      stop("inputs of a covariance function must have the same dimension")
    }

    r <- pracma::Norm(x-y)

    k <- exp(-(r/l)^gamma)
    return(k)
  }

  return(Vectorize(f))

}






##########################################
#'exponential-covariance function-fabric
#'
#'This function-fabric produces the exponential covariance function with the parameter \code{l}.
#'
#'This is a special version of the gamma-exponential-covariance function-fabric when \code{gamma} is 1.
#'See more in \code{?gamma_exp_cov}.\cr
#'\cr
#'Attention: This function only produces covariance functions and can not be directly used as a covariance function.
#'
#'@param l a numeric scale. Default value is 1.
#'@return The covariance function which needs two atomic vector arguments with the same length. It gives the exponential covariance with
#'parameter \code{l}  and that is, \code{exp(-(r/l))} where \code{r} represents the distance between the two vector inputs.\cr
#'\cr
#'The returned function is vectorized can be used in GPR and GPC functions.
#'@examples exp_cov(2)
#' exp_cov(0.5)(1,3)
#' exp_cov(sqrt(exp(1)))(list(c(1,2),c(2,3)),list(5:6))
#'@export
exp_cov<-function(l=1){
  return(gamma_exp_cov(l))
}

##########################################
#'rational-quadratic-covariance function-fabric
#'
#'This function-fabric produces the rational quadratic covariance function with the two parameters \code{l} and \code{alpha}.
#'
#'Attention: This function only produces covariance functions and can not be directly used as a covariance function
#'
#'@param l a numeric scale. Default value is 1.
#'@param alpha a numeric scale. Default value is 1 and value must be in positive.
#'@return The covariance function which needs two atomic vector arguments with the same length. It gives the rational quadratic covariance with
#'parameters \code{l} and \code{alpha} and that is, \code{(1+(r^2)/(2*alpha*l^2))^(-alpha)} where \code{r} represents the distance between the two vector inputs.\cr
#'\cr
#'The returned function is vectorized can be used in GPR and GPC functions.
#'@examples rq_cov(0.2,2)
#' rq_cov(alpha=0.5)(1,3)
#' rq_cov(5,sqrt(exp(1)))(list(c(1,2),c(2,3)),list(5:6))
#'@export
rq_cov <-function(l=1,alpha=1){
  if(length(l)!=1){
    stop("invalid parameter in cov function, l must have length 1")
  }
  if(length(alpha)!=1){
    stop("invalid parameter in cov function, alpha must have length 1")
  }
  if(l<=0){
    stop("invalid parameter in cov function, l must be positive")
  }
  if(alpha<=0){
    stop("invalid parameter in cov function, alpha must be positive")
  }

  f <- function(x,y){
    if(length(x)!=length(y)){
      stop("inputs of a covariance function must have the same dimension")
    }

    r <-pracma::Norm(x-y)
    k <- (1+r^2/(2*alpha*l^2))^(-alpha)
    return(k)
  }
  return(Vectorize(f))
}

#########################################



###########################################
