
######################################################################################################################
#'To produce datas for plots showing data regression function
#'
#'The algorithms used here are exactly the same as the \code{gpr} function. This function makes preparations for
#'the plotting functions. More specifically, given training data and a prior, the \code{gpr} function will be executed
#'for enough suitable test points. The result, which contains a suitable border for the y-axis, those test points and
#'borders for the 95% confidence intervall for each testing point, will be returned as a list.
#'
#'This function only works for one-dimensional test points, since so should the plot functions do. See more details in \code{?gpr}
#'
#'@param inputs a numeric atomic vector which should represent the
#'  training set.
#'@param targets an atomic vevtor which should represent the known scalar outputs
#'  of the traning set.
#'@param covariance_function a function with two arguments which should compute
#'  the covariance between all possible pair of inputs
#'@param noise_level a numeric scale, which represents the the variance of the
#'  Gaussian noise.
#'@return a list. See more in \code{Description}
#'@examples inputs<-1:5
#' targets<-2+runif(5)
#' covariance_function<-se_cov(1)
#' GaussianProcess:::posterior_plot_data(inputs,targets,covariance_function,1)

posterior_plot_data<-function(inputs,targets,covariance_function,noise_level){




  stopifnot("invalid noise level"= length(noise_level)==1 && noise_level>=0)
  if(length(inputs)!=length(targets)){stop("lengths of targets and inputs are not compatible")}
  for(i in 1:length(inputs)){
    stopifnot("plot functions are only available for 1-dimensional inputs" = length(inputs[[i]])==1)
  }
  for(i in 1:length(targets)){
    stopifnot("all targets must be 1-dimensional " = length(targets[[i]])==1)
  }


  stopifnot("Your covariance function is not valid"=is.function(covariance_function))
  start_point<-1.5*min(inputs)-0.5*max(inputs)
  end_point<-1.5*max(inputs)-0.5*min(inputs)


  test_points<-seq(start_point,end_point,by=((end_point-start_point)* 1e-2))
  test_values<-rep(0,length(test_points))
  test_variance<-rep(0,length(test_points))
  K <- outer(inputs,inputs,FUN = covariance_function)
  L <- t(chol(K + noise_level*diag(1,nrow = length(inputs))))
  alpha = solve(t(L),solve(L,targets))
  for(i in 1:length(test_values)){
    temp<-outer(test_points[i],inputs,FUN = covariance_function)
    test_values[i]<-temp%*%alpha
    v<-solve(L,t(temp))
    test_variance[i]<-outer(test_points[i], test_points[i],FUN = covariance_function) - t(v)%*%v
  }
  shadow_lower<-test_values-2*sqrt(abs(test_variance))
  shadow_upper<-test_values+2*sqrt(abs(test_variance))
  lowest<-min(c(shadow_lower,targets))
  highest<-max(c(shadow_upper,targets))
  datas<-data.frame(test_points,test_values,shadow_lower,shadow_upper)

  return(list("highest"=highest,"lowest"=lowest,datas))


}


######################################################################################################################

#'To plot graphics showing posterior distributions after GPR
#'
#'The algorithms used here are exactly the same as the \code{gpr} function. This function is based on the \code{posterior_plot_data}
#'function. More specifically, this function plots the result given by \code{posterior_plot_data}. The graphic contains a red line, which
#'represents the posterior mean values for all suitable test inputs, a shadow area, which represents 95% confidence area for the prediction
#'and also all traning datas as blue "\code{+}".
#'
#'This function only works for one-dimensional test points. See more details in \code{?posterior_plot_data}.
#'
#'@param inputs a numeric atomic vector which should represent the
#'  training set.
#'@param targets an atomic vevtor which should represent the known scalar outputs
#'  of the traning set.
#'@param covariance_function a function with two arguments which should compute
#'  the covariance between all possible pair of inputs
#'@param noise_level a numeric scale, which represents the the variance of the
#'  Gaussian noise.
#'@examples inputs<-1:5
#' targets<-2+runif(5)
#' covariance_function<-se_cov(1)
#' post_plot(inputs,targets,covariance_function,1)
#'@export
post_plot<-function(inputs,targets,covariance_function,noise_level){
  for(i in 1:length(inputs)){
    stopifnot("plot functions are only available for 1-dimensional inputs" = length(inputs[[i]])==1)
  }
  for(i in 1:length(targets)){
    stopifnot("all targets must be 1-dimensional " = length(targets[[i]])==1)
  }
  start_point<-1.5*min(inputs)-0.5*max(inputs)
  end_point<-1.5*max(inputs)-0.5*min(inputs)
  data_list<-posterior_plot_data(inputs,targets,covariance_function,noise_level)
  highest<-data_list[[1]]
  lowest<-data_list[[2]]
  test_points<-data_list[[3]]$test_points
  test_values<-data_list[[3]]$test_values
  shadow_lower<-data_list[[3]]$shadow_lower
  shadow_upper<-data_list[[3]]$shadow_upper

  plot(c(start_point,end_point),c(lowest-0.2*(highest-lowest),highest+0.2*(highest-lowest)),"n",main = "mean and 95% confidence region of the posterior",xlab = "input,x",ylab = "output, f(x)")
  legend("topright",c("traing data","mean values"), lwd =c(NA, 2),# Linienbreite
        pch =c(3, NA),# Symbol
       lty =c(NA, 1),# Linientyp
      col =c("blue", "red"))
  legend("bottom","95% confidence region",fill = "grey70")
  polygon(c(test_points,test_points[length(test_points):1]),c(shadow_lower,shadow_upper[length(shadow_upper):1]),col='grey80',border=NA)
  lines(test_points,test_values,col='red')

  points(inputs,targets,pch=3,col='blue')
  lines(test_points,shadow_lower,col='maroon',lty=2)
  lines(test_points,shadow_upper,col='maroon',lty=2)
}


######################################################################################################################

#'To plot graphics showing prior and posterior distributions before and after GPR
#'
#'The algorithms used here are exactly the same as the \code{gpr} function. This function is based on the \code{post_plot}
#'function. In addition, this function also plots the similar graphic w.r.t the prior.
#'
#'This function only works for one-dimensional test points. See more details in \code{?post_plot}.
#'
#'@param inputs a numeric atomic vector which should represent the
#'  training set.
#'@param targets an atomic vevtor which should represent the known scalar outputs
#'  of the traning set.
#'@param covariance_function a function with two arguments which should compute
#'  the covariance between all possible pair of inputs
#'@param noise_level a numeric scale, which represents the the variance of the
#'  Gaussian noise.
#'@examples inputs<-1:5
#' targets<-2+runif(5)
#' covariance_function<-se_cov(1)
#' prior_post_plot(inputs,targets,covariance_function,1)
#'@export
prior_post_plot<-function(inputs,targets,covariance_function,noise_level){
  for(i in 1:length(inputs)){
    stopifnot("plot functions are only available for 1-dimensional inputs" = length(inputs[[i]])==1)
  }
  for(i in 1:length(targets)){
    stopifnot("all targets must be 1-dimensional " = length(targets[[i]])==1)
  }
  start_point<-1.5*min(inputs)-0.5*max(inputs)
  end_point<-1.5*max(inputs)-0.5*min(inputs)
  data_list<-posterior_plot_data(inputs,targets,covariance_function,noise_level)
  highest<-data_list[[1]]
  lowest<-data_list[[2]]
  test_points<-data_list[[3]]$test_points
  test_values<-data_list[[3]]$test_values
  shadow_lower<-data_list[[3]]$shadow_lower
  shadow_upper<-data_list[[3]]$shadow_upper
  prior_variance<-covariance_function(test_points,test_points)
  prior_shadow_lower<- -2*sqrt(abs(prior_variance))
  prior_shadow_upper<- 2*sqrt(abs(prior_variance))


  par(mfrow=c(1,2))
  plot(c(start_point,end_point),c(lowest-0.2*(highest-lowest),highest+0.2*(highest-lowest)),"n",main = "prior",xlab = "input,x",ylab = "output, f(x)")
  legend("topright",c("traing data","mean values"), lwd =c(NA, 2),# Linienbreite
         pch =c(3, NA),# Symbol
         lty =c(NA, 1),# Linientyp
         col =c("blue", "red"))
  legend("bottom","95% confidence region",fill = "grey70")
  polygon(c(test_points,test_points[length(test_points):1]),c(prior_shadow_lower,prior_shadow_upper[length(prior_shadow_upper):1]),col='grey80',border=NA)
  lines(test_points,rep(0,length(test_points)),col='red')

  points(inputs,targets,pch=3,col='blue')

  lines(test_points,prior_shadow_lower,col='maroon',lty=2)
  lines(test_points,prior_shadow_upper,col='maroon',lty=2)

  plot(c(start_point,end_point),c(lowest-0.2*(highest-lowest), highest+0.2*(highest-lowest)), "n",main = "posterior",xlab = "input,x",ylab = "output, f(x)")
  legend("topright",c("traing data","mean values"), lwd =c(NA, 2),# Linienbreite
         pch =c(3, NA),# Symbol
         lty =c(NA, 1),# Linientyp
         cex = 0.75,
         col =c("blue", "red"))
  legend("bottom","95% confidence region",fill = "grey70")
  polygon(c(test_points,test_points[length(test_points):1]),c(shadow_lower,shadow_upper[length(shadow_upper):1]),col='grey80',border=NA)
  lines(test_points,test_values,col='red')


  points(inputs,targets,pch=3,col='blue')
  lines(test_points,shadow_lower,col='maroon',lty=2)
  lines(test_points,shadow_upper,col='maroon',lty=2)
  par(mfrow=c(1,1))
}

######################################################################################################################


#'To plot graphics showing the posterior w.r.t different covariance functions
#'
#'The algorithms used here are exactly the same as the \code{gpr} function. This function is based on the \code{post_plot}
#'function. The difference is, that this function plots graphic w.r.t different covariance functions.
#'
#'This function only works for one-dimensional test points. See more details in \code{?post_plot}.\cr
#'Attention: This function plots at most for four different covariance functions.
#'
#'@param inputs a numeric atomic vector which should represent the
#'  training set.
#'@param targets an atomic vevtor which should represent the known scalar outputs
#'  of the traning set.
#'@param covariance_functions a (or a list of) function(s) with two arguments which should compute
#'  the covariance between all possible pair of inputs
#'@param noise_level a numeric scale, which represents the the variance of the
#'  Gaussian noise.
#'@examples inputs<-1:5
#' targets<-2+runif(5)
#' covariance_functions<-list(se_cov(1),se_cov(2),rq_cov(1),rq_cov(0.5))
#' cov_fun_compare_plot(inputs,targets,covariance_functions,1)
#'@export
cov_fun_compare_plot<-function(inputs,targets,covariance_functions,noise_level){
  for(i in 1:length(inputs)){
    stopifnot("plot functions are only available for 1-dimensional inputs" = length(inputs[[i]])==1)
  }
  for(i in 1:length(targets)){
    stopifnot("all targets must be 1-dimensional " = length(targets[[i]])==1)
  }
  if(length(covariance_functions)==1 && is.function(covariance_functions)){covariance_functions<-list(covariance_functions)}
  stopifnot("Covariance functions must be given as a list"= is.list(covariance_functions))
  stopifnot("Please give at least 1 and at most 4 covariance functions"=length(covariance_functions)<5 && length(covariance_functions)>0)

  if(length(covariance_functions)==1){par(mfrow=c(1,1))}
  if(length(covariance_functions)==2){par(mfrow=c(1,2))}
  if(length(covariance_functions)==3){par(mfrow=c(2,2))}
  if(length(covariance_functions)==4){par(mfrow=c(2,2))}

  start_point<-1.5*min(inputs)-0.5*max(inputs)
  end_point<-1.5*max(inputs)-0.5*min(inputs)
  if(length(covariance_functions)>2.5) par(cex=0.55)
  if(length(covariance_functions)<2.5) par(cex=0.75)

  for(i in 1:length(covariance_functions)){

    data_list<-posterior_plot_data(inputs,targets,covariance_functions[[i]],noise_level)
    highest<-data_list[[1]]
    lowest<-data_list[[2]]
    test_points<-data_list[[3]]$test_points
    test_values<-data_list[[3]]$test_values
    shadow_lower<-data_list[[3]]$shadow_lower
    shadow_upper<-data_list[[3]]$shadow_upper

    plot(c(start_point,end_point),c(lowest-0.2*(highest-lowest),highest+0.2*(highest-lowest)),"n",main = paste0("posterior w.r.t. ", as.character(i),". cov. function"),xlab = "input,x",ylab = "output, f(x)")
    legend("topright",c("traing data","mean values"), lwd =c(NA, 2),# Linienbreite
           pch =c(3, NA),# Symbol
           lty =c(NA, 1),# Linientyp
           col =c("blue", "red"))
    legend("bottom","95% confidence region",fill = "grey70")
    polygon(c(test_points,test_points[length(test_points):1]),c(shadow_lower,shadow_upper[length(shadow_upper):1]),col='grey80',border=NA)
    lines(test_points,test_values,col='red')

    points(inputs,targets,pch=3,col='blue')
    lines(test_points,shadow_lower,col='maroon',lty=2)
    lines(test_points,shadow_upper,col='maroon',lty=2)

  }
  par(mfrow=c(1,1))
  par(cex=0.75)

}



######################################################################################################################


#'To plot the posterior covariance functions
#'
#'The GPR also gives a poetsrior covariance function, which requires two arguments. In order to show this function two-dimensionally,
#'this function plots the function, when one of the arguments is given in \code{test_inputs} so that there will be only one argument left.
#'
#'This function only works for one-dimensional test points.\cr
#'To make sure that the graphic is clear, graphic will only be drawn for the first six points in \code{test_inputs}.
#'
#'@param inputs a numeric atomic vector which should represent the
#'  training set.
#'@param targets an atomic vevtor which should represent the known scalar outputs
#'  of the traning set.
#'@param covariance_function a function with two arguments which should compute
#'  the covariance between all possible pair of inputs
#'@param noise_level a numeric scale, which represents the the variance of the
#'  Gaussian noise.
#'@param test_inputs a numeric atomic vector, the posterior covariance function related to elements of which will be plotted.
#'@examples inputs<-1:5
#' targets<-2+runif(5)
#' covariance_function<-rq_cov(1)
#' post_cov_plot(inputs,targets,covariance_function,1,c(0.5,1.5,2.5,3.5,4.5,5.5))
#'@export
post_cov_plot<-function(inputs,targets,covariance_function,noise_level,test_inputs){
  if(length(inputs)!=length(targets)){stop("lengths of targets and inputs are not compatible")}
  for(i in 1:length(inputs)){
    stopifnot("plot functions are only available for 1-dimensional inputs" = length(inputs[[i]])==1)
  }
  for(i in 1:length(targets)){
    stopifnot("all targets must be 1-dimensional " = length(targets[[i]])==1)
  }
  stopifnot("Your covariance function is not valid"=is.function(covariance_function))
  if(length(test_inputs)>6){
    test_inputs<-test_inputs[1:6]
    warning("Only the first six of the given test inputs will be plotted.")
  }
  plot_color<-c("darkblue","khaki","darkorange1","green", "deeppink","darkviolet")
  start_point<-1.5*min(inputs)-0.5*max(inputs)
  end_point<-1.5*max(inputs)-0.5*min(inputs)
  test_points<-seq(start_point,end_point,by=((end_point-start_point)* 1e-2))
  all_covariances<-list()
  K <- outer(inputs,inputs,FUN = covariance_function)
  Inv <- solve(K + noise_level*diag(1,nrow = length(inputs)))
  for(i in 1:length(test_inputs)){
    now<-test_inputs[[i]]
    now_covariances<-0
    for (j in 1:length(test_points)) {
      x_star<-c(now,test_points[j])
      K_star<-outer(x_star,inputs, covariance_function)

      now_covariances[j]<-covariance_function(now,test_points[j])-(K_star %*% K %*% t(K_star))[2,1]
    }
    all_covariances[[i]]<-now_covariances
  }

  highest<-max(unlist(all_covariances))
  lowest<-min(unlist(all_covariances))

  plot(c(start_point,end_point),c(lowest-0.2*(highest-lowest),highest+0.2*(highest-lowest)),"n",main = "posterior covariance function with a fixed parameter x'",xlab = "input,x",ylab = "post. cov., cov(f(x),f(x'))")
  legend("topright",paste0("x'=", test_inputs),cex = 0.8, lwd =rep(2,length(test_inputs)),# Linienbreite
         lty =rep(1:6)[1:length(test_inputs)],# Linientyp
         col =plot_color[1:length(test_inputs)])
  for(i in 1:length(test_inputs)){
    lines(test_points,all_covariances[[i]],lwd=3,col=plot_color[i], lty=i)
  }
}


######################################################################################################################



#'To plot the posterior distributions and the posterior covariance functions
#'
#'The function combines the \code{} and \code{} together and creates plots as Figure 2.4 in \code{Rasmussen06}.
#'
#'This function only works for one-dimensional test points.\cr
#'To make sure that the graphic is clear, the second graphic will only be drawn for the first six points in \code{test_inputs}.\cr
#'See more details in \code{?post_plot} and \code{?post_cov_plot}.
#'
#'@param inputs a numeric atomic vector which should represent the
#'  training set.
#'@param targets an atomic vevtor which should represent the known scalar outputs
#'  of the traning set.
#'@param covariance_function a function with two arguments which should compute
#'  the covariance between all possible pair of inputs
#'@param noise_level a numeric scale, which represents the the variance of the
#'  Gaussian noise.
#'@param test_inputs a numeric atomic vector or \code{NULL}. Points, related to which the posterior covariance function will be plotted,
#'  are expected to be given in this vector. By \code{NULL}, suitable test points will be automatically preduced. .
#'@examples inputs<-1:5
#' targets<-2+runif(5)
#' covariance_function<-rq_cov(1)
#' post_postcov_plot(inputs,targets,covariance_function,1,c(0.5,1.5,2.5,3.5,4.5,5.5))
#'@export
post_postcov_plot<-function(inputs,targets,covariance_function,noise_level,test_inputs=NULL){
  par(mfrow=1:2)
  coe1<-
  if(is.null(test_inputs)){
    coe<-c(-0.2,0,0.5,1,1.2)
    test_inputs<-unique(as.integer(coe*max(inputs)+(1-coe)*min(inputs)))
  }

  post_plot(inputs,targets,covariance_function,noise_level)
  post_cov_plot(inputs,targets,covariance_function,noise_level,test_inputs)
  par(mfrow=c(1,1))
}











